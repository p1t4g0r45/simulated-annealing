package draw;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.PaintContext;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.ColorModel;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import binary.AnnealImage;
import binary.MyImage;

public class ChessBoard extends JFrame {

	private MyImage image;
	int size = 20;

	class Fields extends JPanel {

		private void doDrawing(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;

			for (int i = 0; i < AnnealImage.N; i++) {
				for (int j = 0; j < AnnealImage.N; j++) {
					if (image.get(i, j))
						g2d.setPaint(new Color(255, 255, 255));
					else
						g2d.setPaint(new Color(0, 0, 0));
					Rectangle2D r = new Rectangle2D.Double(i * size, j * size,
							size, size);
					g2d.fill(r);
				}
			}

		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			doDrawing(g);
		}
	}

	public ChessBoard(MyImage myimage) {

		this.image = myimage;
		setVisible(true);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				initUI();
			}
		});

	}

	private void initUI() {
		setTitle("Image");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(new Fields());
		setSize(AnnealImage.N * (size+1), AnnealImage.N * (size + 1) + 25);
		setLocationRelativeTo(null);
	}
}
