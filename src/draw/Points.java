package draw;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Stroke;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import komiwojazer.MyPoint;
import komiwojazer.TSP;

public class Points extends JFrame {

	private List<MyPoint> points;

	class Cities extends JPanel {

		private void doDrawing(Graphics g) {

			Graphics2D g2d = (Graphics2D) g;


			int prevx = 0, prevy = 0;
			boolean first = true;

			for (int i = 0; i < points.size(); i++) {
				int x = (int) points.get(i).getX();
				int y = (int) points.get(i).getY();
				if (!first) {
					g2d.setColor(Color.red);
					g2d.setStroke(new BasicStroke(5.0f));
					g2d.drawLine(x, y, x, y);
					g2d.setColor(Color.blue);
					g2d.setStroke(new BasicStroke(1.0f));
					g2d.drawLine(prevx, prevy, x, y);
				}
				prevx = x;
				prevy = y;
				first = false;
			}
		}

		@Override
		public void paintComponent(Graphics g) {

			super.paintComponent(g);
			doDrawing(g);
		}
	}

	public Points(List points) {

		this.points = points;
		setVisible(true);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {

				initUI();
			}
		});

	}

	private void initUI() {

		setTitle("Points");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		add(new Cities());

		setSize(TSP.X, TSP.X);
		setLocationRelativeTo(null);
	}

}
