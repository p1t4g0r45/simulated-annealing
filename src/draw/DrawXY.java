package draw;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.Ellipse2D;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

public class DrawXY extends ApplicationFrame {

	static XYSeries series1;
	static XYSeries series2;
    
    public DrawXY(String title, XYSeries series1, XYSeries series2) {
        super(title);

    	series1 = series1;
        series2 = series2;
        JFreeChart chart = ChartFactory.createXYLineChart(
          title, "", "",
          createDataset(series1, series2), PlotOrientation.VERTICAL,  
          true, true, false //legend, tooltips, url
        );
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 600));
        final XYPlot plot = chart.getXYPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer( );
        renderer.setSeriesPaint(0, Color.RED );
       // renderer.setSeriesLinesVisible(0, false);
        renderer.setSeriesStroke(0, new BasicStroke(1.0f));
        renderer.setSeriesShape(0, new Ellipse2D.Double(-2,-2,4,4));
        if (series2!=null){
            renderer.setSeriesPaint(1, Color.BLUE );
            renderer.setSeriesLinesVisible(1, false);
            renderer.setSeriesStroke(1, new BasicStroke(1.0f));
            renderer.setSeriesShape(1, new Ellipse2D.Double(-2,-2,4,4));
        }
        plot.setRenderer(renderer); 
        setContentPane(chartPanel); 
    }
 
    public static XYDataset createDataset(XYSeries seriesIn, XYSeries seriesIn2) {
    	XYSeriesCollection dataset = new XYSeriesCollection();
    	dataset.addSeries(seriesIn);
    	if (seriesIn2 != null ) dataset.addSeries(seriesIn2);
    	return dataset;
    }

    public static void init (String title, XYSeries series1) {
    	DrawXY demo = new DrawXY(title, series1, null);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }
    
    public static void init (String title, XYSeries series1, XYSeries series2) {
    	DrawXY demo = new DrawXY(title, series1, series2);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

}