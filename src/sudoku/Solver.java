package sudoku;

import java.util.Random;

import org.jfree.data.xy.XYSeries;

import draw.DrawXY;

public class Solver {

	private final String filename = "sudoku.txt";
	public final int N = 9;
	public final int MAX_STEPS = 30000;
	int temperature = MAX_STEPS;

	public static void main(String[] args) {
		new Solver();
	}

	public Solver() {
		Sudoku sudoku = new Sudoku();
		MyFileReader mfr = new MyFileReader(sudoku, filename);
		if (!mfr.fillSudoku()) {
			System.out.println("Wrong input file");
			return;
		}

		System.out.println("Start: ");
		sudoku.print();
		System.out.println("iteration count: " + solve(sudoku));
		System.out.println("beginning with: " + sudoku.getStarting());
	}

	public double getP(double solutionEnergy, double neighbourEnergy) {
		return Math.exp((solutionEnergy - neighbourEnergy) * Math.pow(10, 2) *3
				/ Math.sqrt(temperature));
	}

	private int solve(Sudoku sudoku) {
		sudoku.fillRest();
		//System.out.println(sudoku.getEnergy());

		Random rand = new Random();
		XYSeries series = new XYSeries("Wyzarzanie");

		int i;
		for (i = 0; i < MAX_STEPS; i++) {
			if(temperature==0) break;
			temperature = (int)(0.9999*temperature);
			int first;
			while (sudoku.isUntouchable(first = rand.nextInt(N * N)))
				;
			int second;
			while ((second = (rand.nextInt(3) + (first / 27) * 3) * 9
					+ (rand.nextInt(3) + ((first % 9) / 3) * 3)) == first
					|| sudoku.isUntouchable(second))
				;
			double sum = sudoku.getEnergy();
			if (sum == 0) {
				break;
			}
			sudoku.swap(first, second);
			double sum2 = sudoku.getEnergy();

			if (sum2 > sum) {
				// System.out.println(temperature + " " + sum + " " + sum2 + " "
				// + getP(sum, sum2));
				if (rand.nextDouble() > getP(sum, sum2)) {
					sudoku.swap(second, first); // reverse change
				} else {// else -> the permutation is worse, but we still take
						// it
					// System.out.println(temperature + " worse");
				}
			} // else -> after permutation, result is better (we keep it)

			series.add(i, sudoku.getEnergy());
		}

		DrawXY.init("Wyzarzanie", series);

		System.out.println("Result: ");
		sudoku.print();
		//System.out.println(sudoku.getEnergy());
		return i;

	}

}
