package sudoku;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MyFileReader {

	private Sudoku sudoku;
	File f;

	public MyFileReader(Sudoku sudoku, String filename) {
		this.sudoku = sudoku;
		f = new File(filename);
	}

	public boolean fillSudoku() {
		int count = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			String line;
			int i = 0, x;
			while ((line = br.readLine()) != null) {
				if (line.length() != 9)
					return false;
				for (int j = 0; j < 9; j++) {
					try {
						if (line.charAt(j) == 'x') {
							x = 0;
						} else {
							x = Integer
									.parseInt(String.valueOf(line.charAt(j)));
						}
					} catch (NumberFormatException e) {
						return false;
					}
					if(x!=0){
						count++;
						sudoku.setUntouchable(i,j);
					}
					sudoku.setAt(i, j, x);
				}
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		sudoku.setStarting(count);
		return true;
	}

}
