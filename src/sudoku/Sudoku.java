package sudoku;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Sudoku {

	class Pair<T, S> {
		T x;
		S y;

		public Pair(T x, S y) {
			this.x = x;
			this.y = y;
		}
	}

	private final int N = 9;
	private int starting = 0;

	public int getStarting() {
		return starting;
	}

	public void setStarting(int starting) {
		this.starting = starting;
	}

	private int[][] sudoku;
	private boolean[][] untouchable;

	public Sudoku() {
		sudoku = new int[N][N];
		untouchable = new boolean[N][N];
	}

	public int getAt(int i, int j) {
		return sudoku[i][j];
	}

	public void setAt(int i, int j, int x) {
		sudoku[i][j] = x;
	}

	public void print() {
		System.out.println("------------");
		for (int i = 0; i < sudoku.length; i++) {
			System.out.print("|");
			for (int j = 0; j < sudoku.length; j++) {
				System.out.print(sudoku[i][j]);
				if ((j + 1) % 3 == 0) {
					System.out.print("|");
				}
			}
			System.out.println();
			if ((i + 1) % 3 == 0) {
				System.out.println("------------");
			}
		}
	}

	public int getEnergy() {
		int sum = 0;
		for (int nr = 1; nr <= sudoku.length; nr++) {
			for (int i = 0; i < sudoku.length; i++) {
				int occuranceInRow = 0;
				int occuranceInColumn = 0;
				int occuranceInBlock = 0;
				for (int j = 0; j < sudoku.length; j++) {
					if (sudoku[i][j] == nr) {
						occuranceInRow++;
					}
					if (sudoku[j][i] == nr) {
						occuranceInColumn++;
					}
					if (sudoku[(i * 3) % 9 + j / 3][(i / 3) * 3 + j % 3] == nr) {
						occuranceInBlock++;
					}
				}
				occuranceInRow = occuranceInRow == 0 ? 0 : occuranceInRow - 1;
				occuranceInColumn = occuranceInColumn == 0 ? 0
						: occuranceInColumn - 1;
				occuranceInBlock = occuranceInBlock == 0 ? 0
						: occuranceInBlock - 1;
				sum = sum + occuranceInRow + occuranceInColumn
						+ occuranceInBlock;
			}
		}
		return sum;
	}

	public void fillRest() {
		for (int block = 0; block < 9; block++) {
			Set<Pair<Integer, Integer>> free = new HashSet<>();
			boolean[] occurance = new boolean[10];
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					int x = (block * 3) % 9 + i, y=(block / 3) * 3 + j;
					occurance[sudoku[x][y]] = true;
					if (sudoku[x][y] == 0)
						free.add(new Pair(x, y));
				}
			}
			Iterator<Pair<Integer, Integer>> iterator = free.iterator();
			for (int i = 1; i <= 9; i++) {
				if (!occurance[i]) {
					Pair<Integer, Integer> p = iterator.next();
					sudoku[p.x][p.y] = i;
				}
			}
		}
	}

	public void swap(int first, int second) {
		int tmp = sudoku[first / 9][first % 9];
		sudoku[first / 9][first % 9] = sudoku[second / 9][second % 9];
		sudoku[second / 9][second % 9] = tmp;
	}

	public int getWrong() {
		int sum = 0;
		List<Integer> set = new ArrayList();
		for (int nr = 1; nr <= sudoku.length; nr++) {
			for (int i = 0; i < sudoku.length; i++) {
				int occuranceInRow = 0;
				int occuranceInColumn = 0;
				int occuranceInBlock = 0;
				for (int j = 0; j < sudoku.length; j++) {
					if (sudoku[i][j] == nr) {
						occuranceInRow++;
					}
					if (sudoku[j][i] == nr) {
						occuranceInColumn++;
					}
					if (sudoku[(i * 3) % 9 + j / 3][(i / 3) * 3 + j % 3] == nr) {
						occuranceInBlock++;
					}
					if (occuranceInBlock > 1 || occuranceInColumn > 1
							|| occuranceInRow > 1) {
						set.add(i * 9 + j);
					}
				}

			}
		}
		Random rand = new Random();
		return set.get(rand.nextInt(set.size()));
	}

	public void setUntouchable(int i, int j) {
		untouchable[i][j] = true;
	}

	public boolean isUntouchable(int i) {
		return untouchable[i/9][i%9];
	}

}
