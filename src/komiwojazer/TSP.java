package komiwojazer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.jfree.data.xy.XYSeries;

import draw.DrawXY;
import draw.Points;

public class TSP {

	public static final int X = 500; // wysokosc i szerokosc
	public static final int N = 50; // ilosc punktow
	public static final int MAX_STEPS = 20000;
	int temperature = MAX_STEPS;

	private List<MyPoint> points;

	public static void main(String[] args) {
		TSP m = new TSP();
	}

	public double distanceSum() {
		double sum = 0;
		MyPoint prev = points.get(points.size() - 1);
		for (MyPoint p : points) {
			sum += p.distanceFrom(prev);
			prev = p;
		}
		return sum;
	}

	public void swapPoints(int i, int j) {
		MyPoint copy = points.get(i);
		points.set(i, points.get(j));
		points.set(j, copy);
	}

	public double getP(double solutionEnergy, double neighbourEnergy) {
		return Math.exp((solutionEnergy - neighbourEnergy) * N 
				/ temperature);
	}

	public TSP() {
		XYSeries series = new XYSeries("Wyzarzanie");
		points = new LinkedList<MyPoint>();
		Random rand = new Random();
		for (int i = 0; i < N; i++){
			points.add(new MyPoint(X));
		}
		for (MyPoint p : points) {
			//System.out.println(p);
		}
		new Points(new ArrayList(points));
		System.out.println(distanceSum());

		for (int i = 0; i < MAX_STEPS; i++) {
			if(temperature == 0) break;
			temperature=(int)(0.999*temperature);
			int first = rand.nextInt(N);
			int second;
			while ((second = rand.nextInt(N)) == first)
				;

			double sum = distanceSum();
			swapPoints(first, second);
			double sum2 = distanceSum();

			if (sum2 > sum) {
				//System.out.println(temperature + " " + sum + " " + sum2 + " " + getP(sum, sum2));
				if (rand.nextDouble() > getP(sum, sum2)) {
					swapPoints(second, first); // reverse change
				} else {// else -> the permutation is worse, but we still take
						// it
					//System.out.println(temperature + " worse");
				}
			} // else -> after permutation, result is better (we keep it)

			series.add(i,distanceSum());
		}


		DrawXY.init("Wyzarzanie", series);
		for (MyPoint p : points) {
			//System.out.println(p);
		}
		System.out.println(distanceSum());
		
		new Points(points);
	}
}
