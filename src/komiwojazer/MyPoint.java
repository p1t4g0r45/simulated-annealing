package komiwojazer;

import java.text.DecimalFormat;
import java.util.Random;

public class MyPoint {

	private double x, y;

	public MyPoint(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public MyPoint(int n) {
		Random rand = new Random();
		this.x = rand.nextDouble() * n;
		this.y = rand.nextDouble() * n;
	}

	public MyPoint(int n, int normalAreas) {
		Random rand = new Random();
		if (normalAreas != 4 && normalAreas != 9)
			throw new IllegalArgumentException();
		int sqrt = (int) Math.sqrt(normalAreas);
		this.x = rand.nextInt(sqrt) * n / sqrt + rand.nextGaussian() * n / 50
				+ n / (2 * sqrt);
		this.y = rand.nextInt(sqrt) * n / sqrt + rand.nextGaussian() * n / 50
				+ n / (2 * sqrt);
	}

	public double distanceFrom(MyPoint other) {
		return Math.sqrt(Math.pow((this.x - other.x), 2)
				+ Math.pow((this.y - other.y), 2));
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	@Override
	public String toString() {
		DecimalFormat dc = new DecimalFormat("#.##");
		return "[" + dc.format(x) + " ; " + dc.format(y) + "] ";
	}

}
