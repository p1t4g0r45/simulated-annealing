package binary;

import java.util.Random;

import org.jfree.data.xy.XYSeries;

import draw.ChessBoard;
import draw.DrawXY;

public class AnnealImage {

	public static void main(String[] args) {
		new AnnealImage();
	}

	public static final int N = 25;
	private final int MAX_STEPS =35000;
	int temperature = MAX_STEPS;


	private double getP(double solutionEnergy, double neighbourEnergy) {
		return Math.exp((solutionEnergy - neighbourEnergy) * N  / temperature);
	}
	
	public AnnealImage() {

		XYSeries series = new XYSeries("Wyzarzanie");
		MyImage image = new MyImage(N, 0.25);
		Random rand = new Random();
		System.out.println(getEnergy(image));

		for (int i = 0; i < MAX_STEPS; i++) {
			if(temperature==1) break;
			temperature=(int)(0.999*temperature);
			double sum = getEnergy(image);
			int one = image.getRandom(true);
			int two = image.getRandom(false);
			image.reverse(one, two);
			double sum2 = getEnergy(image);

			if (sum2 > sum) {
				// System.out.println(temperature + " " + sum + " " + sum2 + " "
				// +
				// getP(sum, sum2));
				if (rand.nextDouble() > getP(sum, sum2)) {
					image.reverse(one, two);// reverse change
				} else {// else -> the permutation is worse, but we still take
						// it
					// System.out.println(temperature + " worse");
				}
			} // else -> after permutation, result is better (we keep it)
			series.add(i,getEnergy(image));
		}
		System.out.println(getEnergy(image));
		ChessBoard board = new ChessBoard(image);
		DrawXY.init("Wyzarzanie", series);
	}


	private double getEnergy(MyImage image) {
		double sum = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				sum += image.getEnergy9(i, j);
			}
		}
		return sum;
	}

}
