package binary;

import java.util.Random;

public class MyImage {

	private final int N;
	private boolean[][] image;

	public MyImage(int n, double delta) {
		if (delta > 1 || delta < 0)
			throw new IllegalArgumentException();
		N = n;
		image = new boolean[n][n];

		generateImage(delta);
	}

	private void generateImage(double delta) {
		Random rand = new Random();
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++)
				image[i][j] = rand.nextDouble() > delta;

	}

	public boolean get(int i, int j) {
		return image[i][j];
	}

	public double getEnergy9(int x, int y) {
		double sum = 0;
		for (int i = x - 1; i <= x + 1; i++) {
			for (int j = y - 1; j <= y + 1; j++) {
				if (i < 0 || i >= N || j < 0 || j >= N)
					continue;
				sum += image[i][j] == image[x][y] ? 0 : N;
			}
		}
		return sum;
	}
	
	public double getEnergy25(int x, int y) {
		double sum = 0;
		for (int i = x - 2; i <= x + 2; i++) {
			for (int j = y - 2; j <= y + 2; j++) {
				if (i < 0 || i >= N || j < 0 || j >= N)
					continue;
				sum += image[i][j] == image[x][y] ? 0 : N;
			}
		}
		return sum;
	}

	public double getEnergyLeft(int x, int y) {
		double sum = 0;
		for (int i = x - 1; i <= x; i++) {
			for (int j = y - 1; j <= y; j++) {
				if (i < 0 || i >= N || j < 0 || j >= N)
					continue;
				sum += image[i][j] == image[x][y] ? 0 : N;
			}
		}
		return sum;
	}
	
	public double getEnergyOpposite9(int x, int y) {
		double sum = 0;
		for (int i = x - 1; i <= x + 1; i++) {
			for (int j = y - 1; j <= y + 1; j++) {
				if (i < 0 || i >= N || j < 0 || j >= N)
					continue;
				sum += image[i][j] == image[x][y] ? N : 0;
			}
		}
		return sum;
	}
	
	public double getEnergy45degree(int x, int y) {
		double sum = 0;
		for (int i = x - 1; i <= x + 1; i+=2) {
			for (int j = y - 1; j <= y + 1; j+=2) {
				if (i < 0 || i >= N || j < 0 || j >= N)
					continue;
				sum += image[i][j] == image[x][y] ? 0 : N;
			}
		}
		return sum;
	}
	
	
	public double getEnergy90degree(int x, int y) {
		double sum = 0;
		for (int i = x - 1; i <= x + 1; i+=2) {
			for (int j = y - 1; j <= y + 1; j+=2) {
				if (i < 0 || i >= N || j < 0 || j >= N)
					continue;
				sum += image[i][j] == image[x][y] ? N : 0;
			}
		}
		return sum;
	}
	
	public int getRandom(boolean b) {
		Random rand = new Random();
		int point;
		do {
			point = rand.nextInt(N * N);
		} while (image[point / N][point % N] != b);
		return point;
	}

	public void reverse(int one, int two) {
		image[one/N][one%N]=!image[one/N][one%N];
		image[two/N][two%N]=!image[two/N][two%N];
	}
}
